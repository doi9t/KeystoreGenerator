/*
 *    Copyright 2014 - 2020 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.keystore.generator.exceptions;

public class KeyPairGenerationException extends GenerationException {
    private static final long serialVersionUID = -3255886195392819230L;

    public KeyPairGenerationException(Exception e) {
        super(e);
    }

    public KeyPairGenerationException(String message) {
        super(message);
    }
}
